package comparableandcomparator;

import java.util.Date;


public class Person implements Comparable<Person> {

    public enum Sex {
        MALE, FEMALE, UNDEFINED
    }

    private String firstName;
    private String lastName;
    private Sex sex;
    private Date dateOfBirth;

    public static Person newInstance() {
        return new Person("","",Sex.UNDEFINED,new Date());
    }

    public static Person newInstance(String firstName, String lastName, Sex sex, Date dateOfBirth) {
        return new Person(firstName,lastName,sex,dateOfBirth);
    }

    private Person(String firstName, String lastName, Sex sex, Date dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Sex getSex() {
        return sex;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public int compareTo(Person person) {
        return compareByLastNameAndFirstName(this, person);
    }

    public int compareByLastNameAndFirstName(Person person, Person other) {
        if(person.getLastName().compareTo(other.getLastName()) == 0) {
            return person.getFirstName().compareTo(other.getFirstName());
        }
        return person.getLastName().compareTo(other.getLastName());
    }

    @Override
    public String toString() {
        return firstName + ' ' + lastName;
    }
}
