package comparableandcomparator;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class PersonTest {
    public static void main(String[] args) {
        test(getPrintStream());
    }

    private static void test(PrintStream printStream ) {
        println("Unsorted persons list: ", printStream);
        PersonTest.newInstance().newPersonsList().forEach(p -> println(p, printStream));
        println(printStream);

        println("Sorted by lastName and firstName using comparator: ", printStream);
        List<Person> persons = PersonTest.newInstance().newPersonsList();
        Collections.sort(persons, new LastNameFirstNameComparator());
        persons.forEach(p -> println(p, printStream));
        println(printStream);

        println("Sorted by lastName and firstName using comparable: ", printStream);
        PersonTest.newInstance().newPersonsCollection().forEach(p -> println(p, printStream));
        println(printStream);

        println("Sorted by lastName and firstName using custom method: ", printStream);
        final List<Person> persons1 = PersonTest.newInstance().newPersonsList();
        Collections.sort(persons1, Person.newInstance()::compareByLastNameAndFirstName);
        persons1.forEach(p -> getPrintStream().println(p));
    }

    private static PrintStream getPrintStream() {
        return System.out;
    }

    private static void println(PrintStream printStream) {
        println("", printStream);
    }

    public static PersonTest newInstance() {
        return new PersonTest();
    }

    private PersonTest() {
    }

    private static void println(Object object, PrintStream printStream) {
        printStream.println(object);
    }

    private static class LastNameFirstNameComparator implements Comparator<Person> {
        @Override
        public int compare(Person o1, Person o2) {
            return o1.compareTo(o2);
        }
    }
    
    public List<Person> newPersonsList() {
        List<Person> result = new ArrayList<>();
        result.add(Person.newInstance("John","Smith", Person.Sex.MALE, new Date()));
        result.add(Person.newInstance("Steve","Johnson", Person.Sex.MALE, new Date()));
        result.add(Person.newInstance("Liam","Williams", Person.Sex.MALE, new Date()));
        result.add(Person.newInstance("David","Jones", Person.Sex.MALE, new Date()));
        result.add(Person.newInstance("Charlotte","Jones", Person.Sex.FEMALE, new Date()));
        result.add(Person.newInstance("Emma","Miller", Person.Sex.FEMALE, new Date()));
        return result;
    }
    
    public Collection<Person> newPersonsCollection() {
        Set<Person> result = new TreeSet<>();
        result.addAll(newPersonsList());
        return result;
    }
}
