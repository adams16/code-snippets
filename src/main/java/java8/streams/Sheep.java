package java8.streams;


public class Sheep {
    private String name;

    public Sheep(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Sheep " + name;
    }
}
