package java8.streams;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class SimpleStream {
    public static void main(String[] args) {
        PrintStream printStream = System.out;

        streamOf(printStream);
        integerRange(printStream);
        removeIf(printStream);
        map(printStream);
        mapUsingMethodReference(printStream);
        rangeAndReduce(printStream);
        rangeAndSum(printStream);
        allMatch(printStream);
        anyMatch(printStream);
        printStream.print(combine(words()));
    }

    private static void streamOf(PrintStream printStream) {
        Stream
                .of("cat", "dog", "elephant", "horse")
                .filter(s -> s.contains("a"))
                .forEach(s -> printStream.println(s));
    }

    private static void integerRange(PrintStream printStream) {
        IntStream
                .range(1, 10)
                .filter(a -> a % 2 == 1)
                .forEach(a -> printStream.println(a));
    }

    private static void removeIf(PrintStream printStream) {
        Collection<String> words = words();
        words.removeIf(w -> w.toLowerCase().startsWith("c"));
        words
                .stream()
                .forEach(w -> printStream.println(w));
    }

    private static void map(PrintStream printStream) {
        words()
                .stream()
                .map(w -> new Sheep(w))
                .collect(Collectors.toList())
                .forEach(s -> printStream.println(s));
    }

    private static void mapUsingMethodReference(PrintStream printStream) {
        words()
                .stream()
                .map(Sheep::new)
                .collect(Collectors.toList())
                .forEach(s -> printStream.println(s));
    }

    private static void rangeAndReduce(PrintStream printStream) {
        printStream.println(IntStream
                .range(1, 101)
                .reduce(0, Integer::sum));
    }

    private static void rangeAndSum(PrintStream printStream) {
        printStream.println(IntStream
                .range(1, 101).sum());
    }

    private static void allMatch(PrintStream printStream) {
        printStream.println(words().stream().allMatch(p -> p.toLowerCase().startsWith("c")));
    }

    private static void anyMatch(PrintStream printStream) {
        printStream.println(words().stream().anyMatch(p->p.toLowerCase().startsWith("c")));
    }

    private static String combine(Collection<String> words) {
        return words.stream().filter(w -> !w.isEmpty()).collect(joining(", "));
    }

    private static Collection<String> words() {
        List<String> result = new ArrayList<>();
        result.add("");
        result.add("Cat");
        result.add("Dog");
        result.add("House");
        result.add("City");
        result.add("Window");
        result.add("Elephant");
        result.add("Wall");
        result.add("Car");
        result.add("Elevator");
        result.add("Cloud");
        return result;
    }
}
