package properties;

class MyProperties {

    public static void main(String[] args){
        System.getProperties().stringPropertyNames().forEach(p -> System.out.println(p));
    }
}