package algorithms.tree.binary;

import algorithms.tree.binary.algorithm.converter.BinaryTreeInorderConverter;
import algorithms.tree.binary.algorithm.converter.BinaryTreePostorderConverter;
import algorithms.tree.binary.algorithm.converter.BinaryTreePreorderConverter;
import algorithms.tree.binary.data.BinaryTree;
import algorithms.tree.binary.data.BinaryTreeNode;

import java.io.PrintStream;


public class BinaryTreeMain {

    public static void main(String[] args) {
        PrintStream printStream = System.out;

        new BinaryTreePreorderConverter().toCollection(buildBinaryTree()).forEach(v -> printStream.print(v + getSeparator()));
        printStream.println();

        new BinaryTreeInorderConverter().toCollection(buildBinaryTree()).forEach(v -> printStream.print(v + getSeparator()));
        printStream.println();

        new BinaryTreePostorderConverter().toCollection(buildBinaryTree()).forEach(v -> printStream.print(v + getSeparator()));
        printStream.println();
    }

    private static String getSeparator() {
        return ", ";
    }

    private static BinaryTree<Integer> buildBinaryTree() {
        return new BinaryTree<>(new BinaryTreeNode<>(10,
                    new BinaryTreeNode<>(5, new BinaryTreeNode<>(2), new BinaryTreeNode<>(8)),
                    new BinaryTreeNode<>(15)));
    }
}
