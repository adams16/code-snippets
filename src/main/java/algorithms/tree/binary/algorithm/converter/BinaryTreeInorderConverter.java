package algorithms.tree.binary.algorithm.converter;

import algorithms.tree.binary.data.BinaryTree;
import algorithms.tree.binary.data.BinaryTreeNode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class BinaryTreeInorderConverter implements BinaryTreeConverter {

    @Override
    public <T> Collection<T> toCollection(BinaryTree<T> tree) {
        List<T> result = new ArrayList<>();
        preoder(tree.getRoot(), result);
        return result;
    }

    private <T> void preoder(BinaryTreeNode<T> root, Collection<T> values) {
        if(root.getLeft() != null) {
            preoder(root.getLeft(), values);
        }
        values.add(root.getValue());
        if(root.getRight() != null) {
            preoder(root.getRight(), values);
        }
    }


}
