package algorithms.tree.binary.algorithm.converter;

import algorithms.tree.binary.data.BinaryTree;

import java.util.Collection;

public interface BinaryTreeConverter {
    <T> Collection<T> toCollection(BinaryTree<T> tree);
}
