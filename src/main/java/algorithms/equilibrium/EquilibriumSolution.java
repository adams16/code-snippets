package algorithms.equilibrium;


import java.util.Arrays;

class EquilibriumSolution {

    public static final int EQUILIBRIUM_NOT_FOUND = -1;

    public int solution(int[] A) {
        if (A.length == 0){
            return EQUILIBRIUM_NOT_FOUND;
        }

        long firstSum = Arrays.stream(A).asLongStream().sum();

        long secondSum = 0;
        for (int equilibrium = 0; equilibrium < A.length; equilibrium++) {
            firstSum -= A[equilibrium];
            if (firstSum == secondSum) {
                return equilibrium;
            }
            secondSum += A[equilibrium];
        }
        return EQUILIBRIUM_NOT_FOUND;
    }

}
