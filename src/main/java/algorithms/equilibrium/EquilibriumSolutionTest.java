package algorithms.equilibrium;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;


@RunWith(Parameterized.class)
public class EquilibriumSolutionTest {

    private int[] testData;
    private int expectedResult;
    private EquilibriumSolution testedInstance;

    @Before
    public void setUp() {
        testedInstance = new EquilibriumSolution();
    }

    public EquilibriumSolutionTest(int[] data, int result) {
        this.testData = data;
        this.expectedResult = result;
    }

    @Test
    public void test() {
        assertEquals(testedInstance.solution(testData), expectedResult);
    }

    @Parameterized.Parameters
    public static Collection primeNumbers() {
        return Arrays.asList(new Object[][]{
                {new int[]{}, -1},
                {new int[]{1}, 0},
                {new int[]{1, 2}, -1},
                {new int[]{-1, 3, -4, 5, 1, -6, 2, 1}, 1},
                {new int[]{0, 0, 0}, 0},
                {new int[]{0, -1, 0}, 1},
                {new int[]{1082132608, 0, 1082132608}, 1},
                {new int[]{-1, 0}, 0},
                {new int[]{-1, 0, 0}, 0},
                {new int[]{-1, -1, 1}, 0},
                {new int[]{500, 1, -2, -1, 2}, 0},
        });
    }

}
